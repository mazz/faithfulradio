## flutter sandbox dir

cd ~/src/flutter

## setup flutter

../flutter_sdk/bin/flutter upgrade
../flutter_sdk/bin/flutter doctor --android-licenses
../flutter_sdk/bin/flutter config --enable-macos-desktop
../flutter_sdk/bin/flutter devices

## make new app
../flutter_sdk/bin/flutter create testapp

## build/run
../../flutter_sdk/bin/flutter build apk
../../flutter_sdk/bin/flutter run  

## release
../../flutter_sdk/bin/flutter build apk

## helpful 
https://www.instaflutter.com/flutter-tutorials/generate-flutter-release-apk-android/

