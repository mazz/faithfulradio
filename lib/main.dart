import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_radio_player/flutter_radio_player.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/link.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  final playerState = FlutterRadioPlayer.flutter_radio_paused;

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {

  FlutterRadioPlayer _flutterRadioPlayer = new FlutterRadioPlayer();

  bool isPlaying = false;
  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    initRadioService();
  }

  Future<void> initRadioService() async {
    try {
      await _flutterRadioPlayer.init(
        "Faithful Radio",
        "Hymns",
        "https://radio.japheth.ca/radio/8000/radio_hi.mp3",
        "false", // play when ready
      );
    } on PlatformException {
      print("Exception occurred while trying to register the services.");
    }
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Faithful Radio',
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
          appBar: new AppBar(
            title: const Text('Faithful Radio'),
            backgroundColor: Colors.blueGrey.shade900,
            centerTitle: true,
          ),
          body: Container(
            color: Colors.blueGrey.shade900,
            child: new Column(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Icon(
                      Icons.radio, size: 250,
                      color: Colors.white,
                    ),
                  ),
                  StreamBuilder<String?>(
                    initialData: "",
                    stream: _flutterRadioPlayer.metaDataStream,
                    builder: (context, snapshot) {
                      String? returnData = snapshot.data;

                      if (returnData != null) {
                         return Center(
                          child: Text(returnData, 
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white,
                                                fontSize: 24)
                            ),
                        );
                      }
                      return Text("");
                    },
                  ),
                  SizedBox(height: 20,),
                  Link(
                    uri: Uri.parse('https://radio.japheth.ca/public/faithful_radio'),
                    //target: LinkTarget.self,
                    builder: (context, followLink) {
                      return RichText(
                        text: TextSpan(children: [
                          TextSpan(
                            text: 'Go to Faithful Radio',
                            style: TextStyle(
                              color: Colors.blue,
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              fontSize: 21,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = followLink,
                          ),
                        ]),
                      );
                    }),
                  SizedBox(height: 20,),
                  StreamBuilder(
                    stream: _flutterRadioPlayer.isPlayingStream,
                    initialData: widget.playerState,
                    builder:
                        (BuildContext context, AsyncSnapshot<String?> snapshot) {
                      String? returnData = snapshot.data;
                      if (returnData != null) {
                        print("object data: " + returnData);
                        switch (returnData) {
                          case FlutterRadioPlayer.flutter_radio_stopped:
                            print("flutter_radio_stopped");
                            // return Text("flutter_radio_stopped", 
                            //         style: TextStyle(color: Colors.white,
                            //                         fontSize: 24));
                            break;
                          case FlutterRadioPlayer.flutter_radio_loading:
                            print("flutter_radio_loading");
                            return Text("Loading stream...", 
                                    style: TextStyle(color: Colors.white,
                                                    fontSize: 24));
                            break;
                          case FlutterRadioPlayer.flutter_radio_error:
                            print("flutter_radio_error");
                            // return Text("flutter_radio_error", 
                            //         style: TextStyle(color: Colors.white,
                            //                         fontSize: 24));
                            break;
                          default:
                            print("returnData default");
                            // return Text("returnData default", 
                            //         style: TextStyle(color: Colors.white,
                            //                         fontSize: 24));
                            return Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 40),
                                child: Align(
                                  alignment: FractionalOffset.center,
                                  child: IconButton(
                                    onPressed: () async {
                                      print("button press data: " +
                                          snapshot.data.toString());
                                      await _flutterRadioPlayer.playOrPause();
                                    },
                                    icon: snapshot.data ==
                                            FlutterRadioPlayer.flutter_radio_playing
                                        ? Icon(
                                            Icons.pause_circle_outline,
                                            size: 80,
                                            color: Colors.white,
                                          )
                                        : Icon(
                                            Icons.play_circle_outline,
                                            color: Colors.white,
                                            size: 80,
                                          ),
                                  ),
                                ),
                              )
                            );
                            break;
                        }
                      }
                      // returnData is null
                      return Text("flutter_radio_error");
                    }
                  ),
                  SizedBox(height: 50,)
                ],
            ),
          ),
        ));
  }

}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
